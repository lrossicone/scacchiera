#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scacchiera.h"

void clrscr()
{
	system("@cls||clear");
}

char *getTipo(PEDINA pedina)
{
	if (pedina->tipo == NULL)
		return " ";
	return pedina->tipo;
}

char *getColore(PEDINA pedina)
{
	if (pedina->colore == NULL)
		return " ";
	return pedina->colore;
}

char *getNumero(PEDINA pedina)
{
	if (pedina->numero == 0)
		return " ";
	else if (pedina->numero == 1)
		return "1";
	else if (pedina->numero == 2)
		return "2";
	else if (pedina->numero == 3)
		return "3";
	else if (pedina->numero == 4)
		return "4";
	else if (pedina->numero == 5)
		return "5";
	else if (pedina->numero == 6)
		return "6";
	else if (pedina->numero == 7)
		return "7";
	else if (pedina->numero == 8)
		return "8";
	else if (pedina->numero == 9)
		return "9";
	else
		return " ";
}

int getPosO(char a)
{
	if (a == 'a')
		return 2;
	if (a == 'b')
		return 3;
	if (a == 'c')
		return 4;
	if (a == 'd')
		return 5;
	if (a == 'e')
		return 6;
	if (a == 'f')
		return 7;
	if (a == 'g')
		return 8;
	if (a == 'h')
		return 9;
	else
		return 0; //not a char.
}

void stampaPedina(PEDINA pedina)
{
	printf("%s%s%s ", getTipo(pedina), getColore(pedina), getNumero(pedina));
}

void stampaVirtuale(SCACCHIERA scacchiera)
{
	for (int i = 0; i < 12; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			printf(" %d  ", scacchiera->scacchiera[i][j]);
		}
		printf("\n\n");
	}
}
void stampaScacchiera(SCACCHIERA scacchiera)
{
	for (int i = 0; i < 12; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			if (scacchiera->tavola[i][j] != NULL)
				stampaPedina(scacchiera->tavola[i][j]);
			else
			{
				if (i == 0 || i == 11)
					printf("    ");
				else if (j == 0 || j == 11)
					printf("    ");

				else if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0))
					printf(" *  ");
				else
					printf("    ");
			}
		}
		printf("\n\n");
	}
	printf("\n\n\n");
}
