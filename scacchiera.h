typedef struct posizione
{
	char posO;
	int posV;
} Posizione;
typedef Posizione *POSIZIONE;

typedef struct pedina
{
	char *tipo;
	char *colore;
	Posizione pos;
	int numero, flag;
} Pedina;
typedef Pedina *PEDINA;

typedef struct s
{
	PEDINA tavola[12][12];
	int scacchiera[12][12];
} Scacchiera;
typedef Scacchiera *SCACCHIERA;

SCACCHIERA init();
void stampaScacchiera(SCACCHIERA s);
void muoviPedine(SCACCHIERA s);
void clrscr();
void stampaVirtuale(SCACCHIERA s);
void setPosizione(SCACCHIERA s, PEDINA p, int Orizzontale, int Verticale);
PEDINA creaPedoneBianco(int num);
PEDINA creaPedoneNero(int num);
char *getTipo(PEDINA pedina);
char *getColore(PEDINA pedina);
char *getNumero(PEDINA pedina);
int getPosO(char a);
void stampaPedina(PEDINA pedina);
void test();
PEDINA creaBordoLettera(char *lettera);
PEDINA creaBordoNumero(int num);
