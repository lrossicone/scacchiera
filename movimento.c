#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scacchiera.h"

void setPosizione(SCACCHIERA s, PEDINA p, int Orizzontale, int Verticale)
{
	s->tavola[Orizzontale][Verticale] = p;
	s->scacchiera[Orizzontale][Verticale] = 1;
}

void muoviPedoneBianco(SCACCHIERA s, PEDINA p, POSIZIONE c)
{

	/*Acquisizione nuove Coordinate*/
	int posGot = getPosO(c->posO);
	printf("\nHai selezionato la pedina: ");
	stampaPedina(p);
	printf("\nDove vuoi spostarla? (Inserisci le coordinate della casella di destinazione)\n");
	char O;
	int V;
	printf("Lettera: ");
	scanf("%c%*c", &O);
	printf("\nNumero: ");
	scanf("%d%*c", &V);
	int posGot2 = getPosO(O);

	/*Spostamento del pedone*/
	if (s->scacchiera[posGot2][V + 1] == 0)
	{
		if (p->flag == 0)
		{
			if ((V == c->posV) && ((posGot2 == (posGot - 2)) || ((posGot2 == (posGot - 1)))))
			{
				setPosizione(s, p, posGot2, V + 1);
				p->flag = 1;
				s->tavola[posGot][c->posV + 1] = NULL;
				s->scacchiera[posGot][c->posV + 1] = 0;
				free(c);
			}
			else
			{
				printf("Posizione non valida\n");
				getchar();
			}
		}
		else if (p->flag == 1)
		{
			if ((V == c->posV) && (posGot2 == (posGot - 1)))
			{
				setPosizione(s, p, posGot2, V + 1);
				s->tavola[posGot][c->posV + 1] = NULL;
				s->scacchiera[posGot][c->posV + 1] = 0;
				free(c);
			}
			else
			{
				printf("Posizione non valida\n");
				getchar();
			}
		}
	}
	else
	{
		printf("Posizione occupata\n");
		getchar();
	}
}

void muoviPedoneNero(SCACCHIERA s, PEDINA p, POSIZIONE c)
{

	/*Acquisizione nuove Coordinate*/
	int posGot = getPosO(c->posO);
	printf("\nHai selezionato la pedina: ");
	stampaPedina(p);
	printf("\nDove vuoi spostarla? (Inserisci le coordinate della casella di destinazione)\n");
	char O;
	int V;
	printf("Lettera: ");
	scanf("%c%*c", &O);
	int posGot2 = getPosO(O);
	printf("\nNumero: ");
	scanf("%d%*c", &V);

	/*Spostamento del pedone*/
	if (s->scacchiera[posGot2][V + 1] == 0)
	{
		if (p->flag == 0)
		{
			if ((V == c->posV) && ((posGot2 == (posGot + 2)) || ((posGot2 == (posGot + 1)))))
			{
				setPosizione(s, p, posGot2, V + 1);
				p->flag = 1;
				s->tavola[posGot][c->posV + 1] = NULL;
				s->scacchiera[posGot][c->posV + 1] = 0;
				free(c);
			}
			else
			{
				printf("Posizione non valida\n");
				getchar();
			}
		}
		else if (p->flag == 1)
		{
			if ((V == c->posV) && (posGot2 == (posGot + 1)))
			{
				setPosizione(s, p, posGot2, V + 1);
				s->tavola[posGot][c->posV + 1] = NULL;
				s->scacchiera[posGot][c->posV + 1] = 0;
				free(c);
			}
			else
			{
				printf("Posizione non valida\n");
				getchar();
			}
		}
		else
		{
			printf("Posizione occupata\n");
			getchar();
		}
	}
}

void muovi(SCACCHIERA s, PEDINA p, POSIZIONE c)
{
	PEDINA pedoneBiancoFantoccio = creaPedoneBianco(12);
	PEDINA pedoneNeroFantoccio = creaPedoneNero(12);
	if (getTipo(p) == getTipo(pedoneBiancoFantoccio) && getColore(p) == getColore(pedoneBiancoFantoccio))
		muoviPedoneBianco(s, p, c);
	else if (getTipo(p) == getTipo(pedoneNeroFantoccio) && getColore(p) == getColore(pedoneNeroFantoccio))
		muoviPedoneNero(s, p, c);
	else
	{
		printf("Non ho ancora programmato questo pezzo. . .\n");
		getchar();
	}
}

void muoviPedine(SCACCHIERA s)
{
	POSIZIONE c = malloc(sizeof(Posizione));
	printf("Quale pezzo vuoi muovere? (Inserisci le sue coordinate)\n");
	printf("Lettera: ");
	scanf("%c%*c", &c->posO);
	printf("\nNumero: ");
	scanf("%d%*c", &c->posV);
	int posGot;
	posGot = getPosO(c->posO);
	PEDINA p = s->tavola[posGot][c->posV + 1];
	muovi(s, p, c);
}

int ilPedoneMangiaSe(POSIZIONE c, int Orizzontale, int Verticale)
{
	/**
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 * TI TROVAVI QUI, BEN TORNATO. . .
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 *
	 * **/
	return 1;
}
