#include <stdio.h>
#include <stdlib.h>
#include "scacchiera.h"

int main()
{
	SCACCHIERA t = init();
	int scelta = -1;
	stampaScacchiera(t);
	while (scelta != 0)
	{
		printf("\n\nMENU:\n1->Sposta un pezzo\n2->Visualizza la scacchiera\n0->Esci\n\n");
		scanf("%d%*c", &scelta);
		if (scelta == 1)
		{
			muoviPedine(t);
			clrscr();
			stampaScacchiera(t);
		}
		else if (scelta == 2)
		{
			clrscr();
			stampaScacchiera(t);
		}
		else if (scelta == 3)
		{
			clrscr();
			stampaVirtuale(t);
		}
		else if (scelta == 4)
		{
			test();
			break;
		}
		else if (scelta != 0)
			printf("Selezione non valida!\n");
	}
	printf("Sei stanco? Arrivederci!\n\n");
	return 0;
}
