#include <stdio.h>
#include <stdlib.h>
#include "scacchiera.h"

SCACCHIERA init2()
{
	SCACCHIERA scacchiera = malloc(sizeof(Scacchiera));
	for (int i = 0; i < 12; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			scacchiera->scacchiera[i][j] = 0;
		}
	}
	/**CREAZIONE E POSIZIONAMENTO -UTILITIES-******/

	PEDINA pedoneBianco = creaPedoneBianco(1);
	PEDINA pedoneNero = creaPedoneNero(1);

	setPosizione(scacchiera, pedoneBianco, 6, 6);
	setPosizione(scacchiera, pedoneNero, 5, 5);

	/**CREAZIONE BORDI*****/

	PEDINA bA = creaBordoLettera("A");
	PEDINA bB = creaBordoLettera("B");
	PEDINA bC = creaBordoLettera("C");
	PEDINA bD = creaBordoLettera("D");
	PEDINA bE = creaBordoLettera("E");
	PEDINA bF = creaBordoLettera("F");
	PEDINA bG = creaBordoLettera("G");
	PEDINA bH = creaBordoLettera("H");

	setPosizione(scacchiera, bA, 2, 1);
	setPosizione(scacchiera, bB, 3, 1);
	setPosizione(scacchiera, bC, 4, 1);
	setPosizione(scacchiera, bD, 5, 1);
	setPosizione(scacchiera, bE, 6, 1);
	setPosizione(scacchiera, bF, 7, 1);
	setPosizione(scacchiera, bG, 8, 1);
	setPosizione(scacchiera, bH, 9, 1);

	setPosizione(scacchiera, bA, 2, 10);
	setPosizione(scacchiera, bB, 3, 10);
	setPosizione(scacchiera, bC, 4, 10);
	setPosizione(scacchiera, bD, 5, 10);
	setPosizione(scacchiera, bE, 6, 10);
	setPosizione(scacchiera, bF, 7, 10);
	setPosizione(scacchiera, bG, 8, 10);
	setPosizione(scacchiera, bH, 9, 10);

	PEDINA bordo[8];
	for (int i = 0; i < 8; i++)
		bordo[i] = creaBordoNumero(i + 1);
	for (int j = 0; j < 8; j++)
	{
		setPosizione(scacchiera, bordo[j], 1, j + 2);
		setPosizione(scacchiera, bordo[j], 10, j + 2);
	}
	return scacchiera;
}

void test()
{
	clrscr();
	printf("Benvenuto nella batteria di test. . . \n");
	getchar();
	SCACCHIERA t = init2();
	int scelta = -1;
	stampaScacchiera(t);
	while (scelta != 0)
	{
		printf("\n\nMENU:\n1->Sposta un pezzo\n2->Visualizza la scacchiera\n0->Esci\n\n");
		scanf("%d%*c", &scelta);
		if (scelta == 1)
		{
			muoviPedine(t);
			clrscr();
			stampaScacchiera(t);
		}
		else if (scelta == 2)
		{
			clrscr();
			stampaScacchiera(t);
		}
		else if (scelta == 3)
		{
			clrscr();
			stampaVirtuale(t);
		}
		else if (scelta != 0)
			printf("Selezione non valida!\n");
	}
	printf("Sei stanco? Arrivederci!\n\n");
}
