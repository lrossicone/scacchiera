#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scacchiera.h"

PEDINA creaPedoneBianco(int num)
{
	PEDINA pedone = malloc(sizeof(Pedina));
	pedone->tipo = "P";
	pedone->colore = "b";
	pedone->numero = num;
	//pedone->pos = NULL;
	pedone->flag = 0;
	return pedone;
}

PEDINA creaPedoneNero(int num)
{
	PEDINA pedone = malloc(sizeof(Pedina));
	pedone->tipo = "P";
	pedone->colore = "n";
	pedone->numero = num;
	//pedone->pos = NULL;
	pedone->flag = 0;
	return pedone;
}

PEDINA creaTorreBianca(int num)
{
	PEDINA torre = malloc(sizeof(Pedina));
	torre->tipo = "T";
	torre->colore = "b";
	torre->numero = num;
	//torre->pos = NULL;
	torre->flag = 0;
	return torre;
}

PEDINA creaTorreNera(int num)
{
	PEDINA torre = malloc(sizeof(Pedina));
	torre->tipo = "T";
	torre->colore = "n";
	torre->numero = num;
	torre->flag = 0;
	return torre;
}

PEDINA creaAlfiereBianco(int num)
{
	PEDINA alfiere = malloc(sizeof(Pedina));
	alfiere->tipo = "A";
	alfiere->colore = "b";
	alfiere->numero = num;
	alfiere->flag = 0;
	return alfiere;
}

PEDINA creaCavalloBianco(int num)
{
	PEDINA cavallo = malloc(sizeof(Pedina));
	cavallo->tipo = "C";
	cavallo->colore = "b";
	cavallo->numero = num;
	cavallo->flag = 0;
	return cavallo;
}

PEDINA creaAlfiereNero(int num)
{
	PEDINA alfiere = malloc(sizeof(Pedina));
	alfiere->tipo = "A";
	alfiere->colore = "n";
	alfiere->numero = num;
	alfiere->flag = 0;
	return alfiere;
}

PEDINA creaCavalloNero(int num)
{
	PEDINA cavallo = malloc(sizeof(Pedina));
	cavallo->tipo = "C";
	cavallo->colore = "n";
	cavallo->numero = num;
	cavallo->flag = 0;
	return cavallo;
}

PEDINA creaReBianco()
{
	PEDINA re = malloc(sizeof(Pedina));
	re->tipo = "K";
	re->colore = "b";
	re->flag = 0;
	return re;
}

PEDINA creaReNero()
{
	PEDINA re = malloc(sizeof(Pedina));
	re->tipo = "K";
	re->colore = "n";
	re->flag = 0;
	return re;
}

PEDINA creaReginaBianca()
{
	PEDINA re = malloc(sizeof(Pedina));
	re->tipo = "Q";
	re->colore = "b";
	re->flag = 0;
	return re;
}

PEDINA creaReginaNera()
{
	PEDINA re = malloc(sizeof(Pedina));
	re->tipo = "Q";
	re->colore = "n";
	re->flag = 0;
	return re;
}

PEDINA creaBordoLettera(char *lettera)
{
	PEDINA bordoLettera = malloc(sizeof(Pedina));
	bordoLettera->colore = lettera;
	return bordoLettera;
}

PEDINA creaBordoNumero(int num)
{

	PEDINA bordoNumero = malloc(sizeof(Pedina));
	bordoNumero->numero = num;
	return bordoNumero;
}

PEDINA creaPedinaNera()
{
	PEDINA spar = malloc(sizeof(Pedina));
	spar->colore = "n";
	return spar;
}

SCACCHIERA init()
{
	SCACCHIERA scacchiera = malloc(sizeof(Scacchiera));
	for (int i = 0; i < 12; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			scacchiera->scacchiera[i][j] = 0;
		}
	}
	/**CREAZIONE E POSIZIONAMENTO -PEDONI-******/

	PEDINA arrayP[16];

	for (int i = 0; i < 8; i++)
		arrayP[i] = creaPedoneBianco(i + 1);
	for (int j = 0; j < 8; j++)
		setPosizione(scacchiera, arrayP[j], 8, j + 2);

	for (int i = 8; i < 16; i++)
		arrayP[i] = creaPedoneNero(i - 7);
	for (int j = 8; j < 16; j++)
		setPosizione(scacchiera, arrayP[j], 3, j + 2 - 8);

	/**CREAZIONE E POSIZIONAMENTO -TORRI- ******/

	PEDINA arrayT[4];

	for (int i = 0; i < 2; i++)
		arrayT[i] = creaTorreBianca(i + 1);
	for (int i = 2; i < 4; i++)
		arrayT[i] = creaTorreNera(i + 1 - 2);

	setPosizione(scacchiera, arrayT[0], 9, 2);
	setPosizione(scacchiera, arrayT[1], 9, 9);

	setPosizione(scacchiera, arrayT[2], 2, 2);
	setPosizione(scacchiera, arrayT[3], 2, 9);

	/**CREAZIONE E POSIZIONAMENTO -ALFIERI- ******/

	PEDINA arrayA[4];

	for (int i = 0; i < 2; i++)
		arrayA[i] = creaAlfiereBianco(i + 1);
	for (int i = 2; i < 4; i++)
		arrayA[i] = creaAlfiereNero(i + 1 - 2);

	setPosizione(scacchiera, arrayA[0], 9, 4);
	setPosizione(scacchiera, arrayA[1], 9, 7);

	setPosizione(scacchiera, arrayA[2], 2, 4);
	setPosizione(scacchiera, arrayA[3], 2, 7);

	/**CREAZIONE E POSIZIONAMENTO -CAVALLI- ******/

	PEDINA arrayC[4];

	for (int i = 0; i < 2; i++)
		arrayC[i] = creaCavalloBianco(i + 1);
	for (int i = 2; i < 4; i++)
		arrayC[i] = creaCavalloNero(i + 1 - 2);

	setPosizione(scacchiera, arrayC[0], 9, 3);
	setPosizione(scacchiera, arrayC[1], 9, 8);

	setPosizione(scacchiera, arrayC[2], 2, 3);
	setPosizione(scacchiera, arrayC[3], 2, 8);

	/**CREAZIONE E POSIZIONAMENTO -RE e REGINE- ******/

	PEDINA reB = creaReBianco();
	PEDINA reN = creaReNero();
	PEDINA reginaB = creaReginaBianca();
	PEDINA reginaN = creaReginaNera();

	setPosizione(scacchiera, reB, 9, 5);
	setPosizione(scacchiera, reN, 2, 6);
	setPosizione(scacchiera, reginaB, 9, 6);
	setPosizione(scacchiera, reginaN, 2, 5);

	/**CREAZIONE BORDI*****/

	PEDINA bA = creaBordoLettera("A");
	PEDINA bB = creaBordoLettera("B");
	PEDINA bC = creaBordoLettera("C");
	PEDINA bD = creaBordoLettera("D");
	PEDINA bE = creaBordoLettera("E");
	PEDINA bF = creaBordoLettera("F");
	PEDINA bG = creaBordoLettera("G");
	PEDINA bH = creaBordoLettera("H");

	setPosizione(scacchiera, bA, 2, 1);
	setPosizione(scacchiera, bB, 3, 1);
	setPosizione(scacchiera, bC, 4, 1);
	setPosizione(scacchiera, bD, 5, 1);
	setPosizione(scacchiera, bE, 6, 1);
	setPosizione(scacchiera, bF, 7, 1);
	setPosizione(scacchiera, bG, 8, 1);
	setPosizione(scacchiera, bH, 9, 1);

	setPosizione(scacchiera, bA, 2, 10);
	setPosizione(scacchiera, bB, 3, 10);
	setPosizione(scacchiera, bC, 4, 10);
	setPosizione(scacchiera, bD, 5, 10);
	setPosizione(scacchiera, bE, 6, 10);
	setPosizione(scacchiera, bF, 7, 10);
	setPosizione(scacchiera, bG, 8, 10);
	setPosizione(scacchiera, bH, 9, 10);

	PEDINA bordo[8];
	for (int i = 0; i < 8; i++)
		bordo[i] = creaBordoNumero(i + 1);
	for (int j = 0; j < 8; j++)
	{
		setPosizione(scacchiera, bordo[j], 1, j + 2);
		setPosizione(scacchiera, bordo[j], 10, j + 2);
	}
	return scacchiera;
}
